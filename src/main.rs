use std::{
    collections::BTreeMap,
    env,
    fs::{self, DirEntry},
};

fn calc_size(entry: &DirEntry) -> u64 {
    let file_type = entry.file_type().expect(&format!(
        "Expected to be able to read type of {}",
        entry.file_name().to_string_lossy()
    ));
    if file_type.is_dir() {
        let mut total = 0;
        match fs::read_dir(entry.path()) {
            Ok(dir) => {
                for inner_entry in dir {
                    let inner_entry = inner_entry.expect("AAAAAAAAAfile");
                    total += calc_size(&inner_entry);
                }
                total
            }
            Err(why) => {
                println!(
                    "Can't iterate directory '{}' contents: '{}'",
                    entry.file_name().to_string_lossy(),
                    why
                );
                0
            }
        }
    } else {
        match entry.metadata() {
            Ok(meta) => meta.len(),
            Err(why) => {
                println!(
                    "Can't get file '{}' size: '{}'",
                    entry.file_name().to_string_lossy(),
                    why
                );
                0
            }
        }
    }
}

fn scan(root_path: &str) {
    let root_dir = fs::read_dir(root_path).expect("Expected scan directory to be valid");
    let mut by_size: BTreeMap<u64, String> = Default::default();
    for entry in root_dir {
        let entry = entry.expect("Expected to be able to iterate through directory entries");
        let recursed_size = calc_size(&entry);
        by_size.insert(
            recursed_size,
            entry.file_name().to_string_lossy().to_string(),
        );
    }
    for (size, path) in by_size {
        println!("{}: {}", path, size);
    }
}

fn main() {
    let scan_root = env::args()
        .nth(1)
        .expect("Expected scan directory to be passed as an argument");
    println!("scan_root: {scan_root}");
    scan(&scan_root);
}

# Basic convenience tool

I found it tedious to find out which directories were being filled up with huge amounts of junk,
so I started to write a PowerShell script to find the worst offenders but ended up realizing it
was much easier to just do this in Rust. It's here in case I need it again or anyone else finds
it potentially useful as a utility or as a code example.

## Usage example

```
cargo run C:/Users/Seth/AppData/Local/Pavlov/Saved
```
